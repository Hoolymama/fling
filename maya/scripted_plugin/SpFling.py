import sys
import re
from collections import deque
# from OpenGL.GL import *
import maya.api.OpenMaya as om
import maya.api.OpenMayaUI as omui
import maya.api.OpenMayaRender as omr

# import maya.OpenMayaRender as old_omr

import pymel.core as pm

def maya_useNewAPI():
    """Indicate use of api2 objects."""
    pass


PATH_REGEX = re.compile(r'^.*\.(#+)\.[a-zA-Z]+$')
HASH_REGEX = re.compile(r'(#+)')

class spFling(omui.MPxLocatorNode):
    id = om.MTypeId(0x0008002A)
    drawDbClassification = "drawdb/geometry/spFling"
    drawRegistrantId = "spFlingPlugin"

    aPath = None
    aFrame = None
    aColorGain = None
    aOpacityGain = None
    aFrameCache = None

    textureManager = omr.MRenderer.getTextureManager()

    def __init__(self):
        omui.MPxLocatorNode.__init__(self)

    @staticmethod
    def creator():
        return spFling()

    @staticmethod
    def initialize():
        nAttr = om.MFnNumericAttribute()
        typedAttr = om.MFnTypedAttribute()

        spFling.aPath = typedAttr.create("texturePath", "tp", om.MFnData.kString)
        om.MPxNode.addAttribute(spFling.aPath)

        spFling.aFrame = nAttr.create("frame", "fr", om.MFnNumericData.kInt)
        om.MPxNode.addAttribute(spFling.aFrame)

        spFling.aFrameCacheLength = nAttr.create("frameCache", "fc", om.MFnNumericData.kInt)
        nAttr.setMin(1)
        om.MPxNode.addAttribute(spFling.aFrameCacheLength)

        spFling.aColorGain = nAttr.createColor("colorGain", "clg")
        nAttr.default = (1.0, 1.0, 1.0)
        om.MPxNode.addAttribute(spFling.aColorGain)

        spFling.aOpacityGain = nAttr.create("opacityGain", "opg", om.MFnNumericData.kFloat, 1.0)
        nAttr.setSoftMin(0.0)
        nAttr.setSoftMax(1.0)
        om.MPxNode.addAttribute(spFling.aOpacityGain)

    # def excludeAsLocator(self):
    #     return False

    # def isBounded(self):
    #     return True

    # def boundingBox(self):
    #     bb = om.MBoundingBox()
    #     bb.expand(om.MPoint(-1.0,  1.0, 0.0))
    #     bb.expand(om.MPoint( 1.0,  1.0, 0.0))
    #     bb.expand(om.MPoint( 1.0, -1.0, 0.0))
    #     bb.expand(om.MPoint(-1.0, -1.0, 0.0))
    #     return bb

    # def draw(self, view, path, style, status):
    #     ## Get the size
    #     ##
    #     # thisNode = self.thisMObject()
    #     # plug = om.MPlug( thisNode, footPrint.size )
    #     # sizeVal = plug.asMDistance()
    #     # multiplier = sizeVal.asCentimeters()

    #     # global sole, soleCount
    #     # global heel, heelCount

    #     # glFT =omr.MRenderer.glFunctionTable()

    #     glRenderer = old_omr.MHardwareRenderer.theRenderer()
    #     glFT = glRenderer.glFunctionTable()
    #     view.beginGL()

    #     ## Draw the outline of the foot
    #     ##
    #     glFT.glBegin( old_omr.MGL_LINES )

    #     glFT.glVertex3f( -1.0,  1.0, 0.0 )
    #     glFT.glVertex3f(  1.0,  1.0, 0.0 )

    #     glFT.glVertex3f( 1.0,  1.0, 0.0 )
    #     glFT.glVertex3f(  1.0, -1.0, 0.0 )

    #     glFT.glVertex3f(  1.0, -1.0, 0.0 )
    #     glFT.glVertex3f(  -1.0, -1.0, 0.0 )

    #     glFT.glVertex3f(  -1.0, -1.0, 0.0 )
    #     glFT.glVertex3f( -1.0,  1.0, 0.0 )

    #     view.endGL()


##---------------------------------------------------------------------------
##---------------------------------------------------------------------------
## Viewport 2.0 override
##---------------------------------------------------------------------------
##---------------------------------------------------------------------------


class spFlingData(om.MUserData):
    def __init__(self):
        om.MUserData.__init__(self, False)
        # self.fPath = "/Users/julian/projects/fling_texmap/sourceimages/remy_beach.tif"
        self.fTextureName = None
        self.fColor = None
        # self.fFrameCacheLength = None
        
################################################################################################
class spFlingDrawOverride(omr.MPxDrawOverride):

    def __init__(self, obj):
        omr.MPxDrawOverride.__init__(self, obj, spFlingDrawOverride.draw)
        print "INIT spFlingDrawOverride"
        self.textureManager = omr.MRenderer.getTextureManager()
        if  self.textureManager is None:
            pm.warning("TextureManager was not initialized")
        self.frameCache = deque([], 1)
        self.sentinel = None


    @staticmethod
    def creator(obj):
        return spFlingDrawOverride(obj)

    @staticmethod
    def draw(context, data):
        pass

    def supportedDrawAPIs(self):
        return (omr.MRenderer.kOpenGL | omr.MRenderer.kDirectX11)

    def isBounded(self, objPath, cameraPath):
        return True

    def boundingBox(self, objPath, cameraPath):
        bb = om.MBoundingBox()
        bb.expand(om.MPoint(-1.0,  1.0, 0.0))
        bb.expand(om.MPoint( 1.0,  1.0, 0.0))
        bb.expand(om.MPoint( 1.0, -1.0, 0.0))
        bb.expand(om.MPoint(-1.0, -1.0, 0.0))
        return bb

    @staticmethod
    def resolve_filename(filename, frame):
        match = PATH_REGEX.match(filename)
        if match:
            padding = len(match.group(1))
            padder = "%%0%dd" % padding
            frameStr = padder % frame
            return HASH_REGEX.sub(frameStr, filename)
        else:
            return filename

    def prepareForDraw(self, objPath, cameraPath, frameContext, oldData):
        data = oldData
        if not isinstance(data, spFlingData):
            data = spFlingData()

        spFlingNode = objPath.node()
        if spFlingNode.isNull():
            return

        # mipmapLevels = 1;


        plug = om.MPlug(spFlingNode, spFling.aColorGain)
        o = plug.asMObject()
        nData = om.MFnNumericData(o)
        data.fColor = om.MColor(nData.getData())
        plug = om.MPlug(spFlingNode, spFling.aOpacityGain)
        data.fColor.a = plug.asFloat()

        if  self.textureManager is not None:

            # manage the frame cache max length changing
            # notice nothing happens if the length doesn't change
            plug = om.MPlug(spFlingNode, spFling.aFrameCacheLength)
            newCacheLength = max(1,plug.asInt())
            newCache = None
            if newCacheLength > self.frameCache.maxlen:
                # print "increasing frameCache to newCacheLength"
                newCache = deque(list(self.frameCache), newCacheLength)
                # self.frameCache = newCache
            elif newCacheLength < self.frameCache.maxlen:
                # print "decreasing frameCache to newCacheLength"
                newCache = None
                actualLen = len(self.frameCache)
                if actualLen <= newCacheLength:
                    newCache = deque(list(self.frameCache), newCacheLength)
                else:
                    partition = actualLen - newCacheLength
                    newCache = deque(list(self.frameCache)[partition:], newCacheLength)
                    for texture in list(self.frameCache)[:partition]:
                        self.textureManager.releaseTexture(texture)
            if newCache is not None:
                self.frameCache = newCache
                # print "self.frameCache.maxlen: %d" % self.frameCache.maxlen

            # path and frame plugs
            plug = om.MPlug(spFlingNode, spFling.aPath)
            filename = plug.asString()
            plug = om.MPlug(spFlingNode, spFling.aFrame)
            frame = plug.asInt()
            data.fTextureName = self.resolve_filename(filename, frame)
            if data.fTextureName is not None:
                # print "data.fTextureName: %s" % data.fTextureName
                # print "frameCache: %s" % self.frameCache

                if data.fTextureName not in (texture.name() for texture in self.frameCache):
                    # unfortunately we cant just append. We have to first
                    # check if the cache is full and if so manually pop
                    # the oldest so it can be released.
                    if len(self.frameCache) == self.frameCache.maxlen:
                        poppedTexture = self.frameCache.popleft()
                        if poppedTexture:
                            self.textureManager.releaseTexture(poppedTexture)
                    
                    # newTexture = self.textureManager.acquireTexture(data.fTextureName)
                    # print "newTexture.name(): %s" % newTexture.name()
                    self.frameCache.append(self.textureManager.acquireTexture(data.fTextureName))




            # #######################################################
            # If we already know about a texture, check it is the one
            # we are using. If it isn't, then free up the sentinel
            # if self.sentinel is not None:
            #     print "sentinel is not None"
            #     if self.sentinel.name() != data.fTextureName:
            #         print "sentinel.name %s is different to fTextureName %s, releasing sentinel" % (self.sentinel.name(), data.fTextureName)
            #         self.textureManager.releaseTexture(self.sentinel)
            #         self.sentinel = None
            #     else:
            #         print "sentinel.name is same as data.fTextureName"
            # else:
            #     print "sentinel is None"

            # if self.sentinel is None:
            #     self.sentinel = self.textureManager.acquireTexture(data.fTextureName)



        # print  ">>>>> PREP" 

        return data

    def hasUIDrawables(self):
        return True

    def addUIDrawables(self, objPath, drawManager, frameContext, data):
        if not isinstance(data, spFlingData):
            return

        drawManager.beginDrawable()

        mode = omr.MUIDrawManager.kTriangles

        position = om.MPointArray()
        position.append(om.MPoint(-1.0,  1.0, 0.0))
        position.append(om.MPoint( 1.0,  1.0, 0.0))
        position.append(om.MPoint( 1.0, -1.0, 0.0))
        position.append(om.MPoint(-1.0, -1.0, 0.0))

        uvCoord = om.MPointArray()
        uvCoord.append(om.MPoint( 0.0, 1.0, 0.0))
        uvCoord.append(om.MPoint( 1.0, 1.0, 0.0))
        uvCoord.append(om.MPoint( 1.0, 0.0, 0.0))
        uvCoord.append(om.MPoint( 0.0, 0.0, 0.0))

        index = om.MUintArray()
        index.append(0)
        index.append(2)
        index.append(1)
        index.append(0)           
        index.append(3)           
        index.append(2)   


        col = om.MColor(data.fColor)
        drawManager.setColor(col)
        # drawManager.setLineWidth(1)
        theTexture = None
        if data.fTextureName is not None:
            theTexture = self.textureManager.acquireTexture(data.fTextureName)
            if theTexture is not None:
                drawManager.setTextureMask( omr.MBlendState.kRGBAChannels )
                drawManager.setTexture(theTexture );

        drawManager.mesh(mode, position, None, None, index, uvCoord)

        drawManager.setTexture( None )
        drawManager.endDrawable()

        # print "DRAW <<<<<<"
        if  theTexture is not None:
            self.textureManager.releaseTexture(theTexture)
      





def initializePlugin(obj):
    plugin = om.MFnPlugin(obj, "Autodesk", "3.0", "Any")
    try:
        plugin.registerNode("spFling", spFling.id, spFling.creator, spFling.initialize, om.MPxNode.kLocatorNode, spFling.drawDbClassification)
    except:
        sys.stderr.write("Failed to register node\n")
        raise

    try:
        omr.MDrawRegistry.registerDrawOverrideCreator(spFling.drawDbClassification, spFling.drawRegistrantId, spFlingDrawOverride.creator)
    except:
        sys.stderr.write("Failed to register override\n")
        raise

def uninitializePlugin(obj):
    plugin = om.MFnPlugin(obj)
    try:
        plugin.deregisterNode(spFling.id)
    except:
        sys.stderr.write("Failed to deregister node\n")
        raise

    try:
        omr.MDrawRegistry.deregisterGeometryOverrideCreator(spFling.drawDbClassification, spFling.drawRegistrantId)
    except:
        sys.stderr.write("Failed to deregister override\n")
        raise